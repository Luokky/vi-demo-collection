package ru.vibrotek;

import java.util.SortedMap;

/**
 * Created by Luokky on 30.03.2017.
 */
public class ViBiKeyMapTest {
    public void startTest(){
        ViBiKeyMap VBKMapTest = new ViBiKeyMap();

        //Test add
        VBKMapTest.add("val-0-1", 0, 1);
        VBKMapTest.add("val-0-2", 0, 2);
        VBKMapTest.add("val-2-0", 2, 0);
        VBKMapTest.add("val-2-1", 2, 1);
        VBKMapTest.add("val-2-3", 2, 3);
        VBKMapTest.add("val-3-2", 3, 2);
        VBKMapTest.add("val-3-3", 3, 3);
        VBKMapTest.add("val-0-3", 0, 3);
        VBKMapTest.add("val-1-3", 1, 3);
        VBKMapTest.add("val-4-0", 4, 0);
        VBKMapTest.add("val-4-2", 4, 2);

        //Test gets
        System.out.println(VBKMapTest.getAll());
        System.out.println();

        System.out.println(VBKMapTest.get(2,0));
        System.out.println(VBKMapTest.get(0,2));
        System.out.println();

        System.out.println(VBKMapTest.getValuesByCategory(0));
        System.out.println();

        //Test remove by key
        VBKMapTest.removeByCategoryDeep(0);
        System.out.println("After remove by key 0:\n"+VBKMapTest.getAll());
    }
}
