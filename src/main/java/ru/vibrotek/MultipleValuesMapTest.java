package ru.vibrotek;

import java.util.function.Consumer;

public class MultipleValuesMapTest {
    public void startTest() {
        MultipleValuesMap MVPTest = new MultipleValuesMap();

        //Add test
        MVPTest.add(0, "0-0");
        MVPTest.add(0, "0-1");
        MVPTest.add(1, "1-0");
        MVPTest.add(1, "1-1");
        MVPTest.add(0, "0-2");

        //contains key test
        System.out.println(MVPTest.containsKey(0)
                + " " + MVPTest.containsKey(2)
                + " " + MVPTest.containsKey(1));

        //get values by key test
        System.out.println(MVPTest.getValuesByCategory(0));
        System.out.println();

        System.out.println(MVPTest.getValuesByCategory(2));
        System.out.println();

        System.out.println(MVPTest.getValuesByCategory(1));
        System.out.println();

        //test remove by key
        MVPTest.add(2, "removeTest");
        System.out.println("Add data to key 2 : " + MVPTest.getValuesByCategory(2));
        System.out.println();
        MVPTest.remove(2, "wrong data");
        System.out.println("After remove with wrong data : " + MVPTest.getValuesByCategory(2));
        System.out.println();
        MVPTest.remove(2, "removeTest");
        System.out.println("After remove with correct data : " + MVPTest.getValuesByCategory(2));
        System.out.println();
        MVPTest.remove(2, "removeTest");
        System.out.println("After remove without data by key : " + MVPTest.getValuesByCategory(2));
        System.out.println();

        //test remove all data by key
        MVPTest.add(3, "removeAll-1");
        MVPTest.add(3, "removeAll-2");
        MVPTest.add(3, "removeAll-3");
        System.out.println("Add data to key 3 : " + MVPTest.getValuesByCategory(3));
        System.out.println();
        MVPTest.removeAll("wrong key");
        System.out.println("After remove all with wrong data : ");
        System.out.println(MVPTest.getValuesByCategory(0));
        System.out.println(MVPTest.getValuesByCategory(1));
        System.out.println(MVPTest.getValuesByCategory(2));
        System.out.println(MVPTest.getValuesByCategory(3));
        System.out.println(MVPTest.getValuesByCategory("wrong key"));
        System.out.println();
        MVPTest.removeAll(3);
        System.out.println("After remove all with correct data : " + MVPTest.getValuesByCategory(3));
        System.out.println();

        //Test forEachKey
        Consumer<Integer> consumerInt = i -> {
            System.out.println(MVPTest.getValuesByCategory(i));
        };
        System.out.println("sout forEachKey:");
        MVPTest.forEachKey(consumerInt);

        //Test forEachValue
        Consumer<String> consumerStr = s -> {
            System.out.println(s);
        };
        System.out.println("sout forEachValue:");
        MVPTest.forEachValue(consumerStr);

    }
}
