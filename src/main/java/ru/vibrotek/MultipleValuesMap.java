package ru.vibrotek;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

/**
 * Карта содержащая в себе множество значений для одного ключа
 */
public class MultipleValuesMap<K, V> {

    private ArrayList<Entry> elements = new ArrayList<Entry>();

    public void add(K key, V value) {
        if (!containsKey(key)){
            ViList values = new ViList();
            values.add(value);
            Entry entry = new Entry(key, values);
            elements.add(entry);
        } else {
            getEntryByKey(key).addValue(value);
        }

    }

    /**
     * Возвращает все значения для ключа
     * @param key
     * @return
     */
    public List<V> getValuesByCategory(K key) {
        if (containsKey(key)){
            ViList<V> innerVal = new ViList<V>(getEntryByKey(key).getValues());
            ArrayList<V> resVal = new ArrayList<V>();
            for (int i = 0; i<innerVal.size(); i++){
                resVal.add(innerVal.get(i));
            }
            return resVal;
        }
        return null;
    }

    public boolean containsKey(K key) {
        for (Entry entry : elements) {
            if(entry.getKey() == key) return true;
        }
        return false;
    }

    public void remove(K key, V value) {//если список пуск, удалить ключ из карты
        if (containsKey(key)){
            ViList valuesForKey = getEntryByKey(key).getValues();
            if (valuesForKey.contains(value)){
                valuesForKey.remove(value);
            } else if (valuesForKey.size()==0){
                elements.remove(getEntryByKey(key));
            }
        }
    }

    public void removeAll(K key) {
        if (containsKey(key)){
            elements.remove(getEntryByKey(key));
        }

    }

    public void forEachKey(Consumer<K> consumer) {
        if (elements.size()>0) {
            for (int i = 0; i < elements.size(); i++){
                consumer.accept((K) elements.get(i).getKey());
            }
        }
    }

    public void forEachValue(Consumer<V> consumer) {
        if (elements.size()>0) {
            Consumer<K> forEachKeyConsumer = k -> {
                ViList valuesForKey = getEntryByKey(k).getValues();
                Consumer<V> forEachValueConsumer = v -> {
                    consumer.accept(v);
                };
                valuesForKey.forEach(forEachValueConsumer);
            };
            forEachKey(forEachKeyConsumer);
        }
    }

    private Entry getEntryByKey(K key){
        if (containsKey(key)){
            for (Entry entry : elements) {
                if(entry.getKey() == key) return entry;
            }
        }
        return null;
    }

    private class Entry<K,V>{
        private K key;
        private ViList<V> values;

        private Entry(K key, ViList values){
            this.key = key;
            this.values = values;
        }

        private K getKey() {
            return key;
        }

        private void setKey(K key) {
            this.key = key;
        }

        private ViList<V> getValues() {
            return values;
        }

        private void setValues(ViList<V> values) {
            this.values = values;
        }

        private void addValue(V value){
            values.add(value);
        }

        private void removeValue (V value){
            values.remove(value);
        }

        @Override
        public int hashCode() {
            char[] keyArr = key.toString().toCharArray();
            int hashCode = 0;
            for (int i = 0; i< keyArr.length; i++){
                hashCode += keyArr[i]*31^(keyArr.length-i+1);
            }
            return hashCode;
        }

        @Override
        public boolean equals(Object obj) {
            Entry innerEntry = (Entry)obj;
            return hashCode()==innerEntry.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Entry for key ");
            stringBuilder.append(key);
            stringBuilder.append(":\n");
            for (int i = 0; i<values.size(); i++) {
                stringBuilder.append(values.get(i));
                stringBuilder.append("\n");
            }
            return stringBuilder.toString();
        }
    }
}
