package ru.vibrotek;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ViListTest {
    public void startTest() {

        //Проверка конструктора
        ViList<String> listTest = new ViList<>(5);

        //Проверка isEmpty ч.1
        System.out.println("Is listTest empty? " + listTest.isEmpty());

        //Проверка метода add и расширения массива при необходимости
        for (int i = 0; i < 5; i++) {
            listTest.add("node-" + i);
        }

        //Проверка isEmpty ч.2
        System.out.println("Is listTest empty? " + listTest.isEmpty());

        //Проверка forEach
        System.out.println("Print for each:");
        Consumer<String> print = s -> System.out.println(s);
        listTest.forEach(print);

        //Проверка add obj in i position
        listTest.add(2, "insert");
        System.out.println("Print for each:");
        listTest.forEach(print);

        // Проверка метода remove по номеру
        System.out.println("remove 2");
        listTest.remove(2);

        // Проверка метода remove по объекту, метода getFirst, метода contains, метода indexOf, метода isEmpty:
        System.out.println("remove first");
        listTest.remove(listTest.getFirst());

        System.out.println("Print for each:");
        listTest.forEach(print);

        //Проверка методов subList и containsAll
        List<String> subStr = listTest.subList(1, 3);
        Set<String> subStrSet = new HashSet<>();
        for (String s : subStr) {
            subStrSet.add(s);
        }
        System.out.println("listTest conains: " + subStrSet.toString() + "?");
        System.out.println(listTest.containsAll(subStrSet));
        subStrSet.add("Smth Strange");
        System.out.println("And if we add smth strange");
        System.out.println(listTest.containsAll(subStrSet));

        //Проверка метода addAll
        listTest.addAll(subStr);
        System.out.println("Print for each:");
        listTest.forEach(print);

        //Проверка search
        Predicate<String> predicate = (s) -> s.equals("node-1");
        System.out.println("Search node-1 : " + listTest.search(predicate));
        Predicate<String> predicate2 = (s) -> s.equals("smth strange");
        System.out.println("Search smth strange : " + listTest.search(predicate2) == null ? "Null" : listTest.search(predicate2));

        //Проверка метода add(i, obj)
        listTest.add(5, "node");
        System.out.println("Print for each:");
        listTest.forEach(print);
    }
}
