package ru.vibrotek;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 *
 */
public class ViList<T> {

    private T[] elements;  // Элементы коллекции
    private int size = 0; // Количество элементов в клддекции
    private int capacity = 10; //Ёмкость коллекции. По умолчанию 10

    public ViList() {
        initElements();
    }

    public ViList(int capacity) {
        this.capacity = capacity;
        initElements();
    }

    public ViList(ViList<T> list) {
        capacity = list.size();
        initElements();
        for (int i = 0; i < list.size(); i++) {
            add(list.get(i));
            ;
        }
    }

    public void add(T node) {
        ensureCapacity();
        elements[size()] = node;
        size++;
    }

    public void add(ViList<T> list) {
        for (int i = 0; i < list.size(); i++) {
            add(list.get(i));
            ;
        }
    }

    public void clear() {
        size = 0;
        capacity = 10;
        initElements();
    }

    public Iterator iterator() {
        return new Iterator() {
            int cursor = 0;

            @Override
            public boolean hasNext() {
                return cursor >= size();
            }

            @Override
            public Object next() {
                if (hasNext()) {
                    cursor++;
                    return elements[cursor];
                }
                return null;
            }

            @Override
            public void remove() {
                System.arraycopy(elements, cursor + 1, elements, cursor, size - cursor - 1);
            }
        };
    }

    public void forEach(Consumer<T> consumer) {
        for (int i = 0; i < size(); i++) {
            consumer.accept(elements[i]);
        }
    }

    public List<T> toImmutableList() {
        return Collections.unmodifiableList(Arrays.asList(elements));
    }

    public void remove(T selectedItem) {
        if (contains(selectedItem)) {
            remove(indexOf(selectedItem));
        }
    }

    public void remove(int selectedItem) {
        if (size() >= selectedItem) {
            T[] tempData = (T[]) new Object[capacity];
            System.arraycopy(elements, 0, tempData, 0, (selectedItem));
            System.arraycopy(elements, selectedItem + 1, tempData, selectedItem, size() - selectedItem - 1);
            elements = tempData;
            size--;
        }
    }

    public boolean contains(T storage) {
        boolean contains = false;
        for (int i = 0; i < size; i++) {
            if (storage.equals(elements[i])) return true;
        }
        return false;
    }

    public int size() {
        return size;
    }

    public T get(int i) {
        if (size() >= i) return elements[i];
        return null;
    }

    public boolean isEmpty() {
        return (size() == 0);
    }

    public int indexOf(T selectedItem) {
        if (contains(selectedItem)) {
            for (int i = 0; i < size(); i++) {
                if (elements[i].equals(selectedItem)) return i;
            }
        }
        return -1;
    }

    public void add(int i, T selectedItem) {
        ensureCapacity();
        T[] tempData = (T[]) new Object[size - i];
        System.arraycopy(elements, i, tempData, 0, tempData.length);
        elements[i] = selectedItem;
        System.arraycopy(tempData, 0, elements, i + 1, tempData.length);
        size++;
    }

    public void addAll(List<T> contacts) {
        for (T contact : contacts) {
            add(contact);
        }
    }

    public T getLast() {
        if (!isEmpty()) return elements[size() - 1];
        return null;
    }

    public T getFirst() {
        if (!isEmpty()) return get(0);
        return null;
    }

    public T removeFirst() {
        if (!isEmpty()) {
            T firstNode = getFirst();
            remove(0);
            return firstNode;
        }
        return null;
    }

    public T search(Predicate<T> predicate) {
        for (int i = 0; i < size(); i++) {
            if (predicate.test(elements[i])) return elements[i];
        }
        return null;
    }

    public boolean containsAll(Set<T> zones) {
        for (T temp : zones) {
            if (!contains(temp)) return  false;
        }
        return true;
    }

    public List<T> subList(int from, int to) {
        T[] tempData = (T[]) new Object[to - from + 1];
        System.arraycopy(elements, from, tempData, 0, tempData.length);
        return Arrays.asList(tempData);
    }

    private void ensureCapacity() {
        //Выясняем, хватает ли нам ёмкости, если нет, то
        if (size() + 1 > elements.length) {
            capacity = (capacity * 3) / 2 + 1;
            T[] tempData = (T[]) new Object[capacity];
            System.arraycopy(elements, 0, tempData, 0, size());
            elements = tempData;
        }
    }

    private void initElements() {
        elements = (T[]) new Object[capacity];
    }
}
