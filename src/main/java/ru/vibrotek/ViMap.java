package ru.vibrotek;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 *
 */
public class ViMap<K, V> {

    private ViList<K> keys = new ViList<K>();
    private ViList<Entry> elements = new ViList<>();

    public void put(K key, V measuringNode) {
        if (keys.contains(key)) {

            Entry entry = getEntry(key);
            int indexOfEntry = elements.indexOf(entry);
            elements.remove(indexOfEntry);
            entry.setValue(measuringNode);
            elements.add(indexOfEntry, entry);

        }
    }

    public void add(K key, V value) {
        if (!keys.contains(key)) {
            keys.add(key);
            Entry entry = new Entry(key, value);
            elements.add(entry);
        }
    }

    public V remove(K key) {
        if (keys.contains(key)) {
            Entry entry = getEntry(key);
            V result = (V) entry.getValue();
            elements.remove(entry);
            return result;
        }
        return null;
    }

    public boolean containsKey(K key) {
        return keys.contains(key);
    }

    public int size() {
        return elements.size();
    }

    public Map<K, V> toImmutableMap() {
        HashMap<K, V> result = new HashMap<K, V>();
        for (int i = 0; i < elements.size(); i++) {
            result.put((K) elements.get(i).getKey(), (V) elements.get(i).getValue());
        }
        return Collections.unmodifiableMap(result);
    }

    public void forEachValue(Consumer<V> consumer) {
        for (int i = 0; i < elements.size(); i++) {
            consumer.accept((V) elements.get(i).getValue());
        }
    }

    public void forEachKey(Consumer<K> consumer) {
        for (int i = 0; i < elements.size(); i++) {
            consumer.accept((K) elements.get(i).getKey());
        }
    }

    public void removeIf(Predicate<V> predicate) {

        for (int i = elements.size() - 1; i >= 0; i--) {
            if (predicate.test((V) elements.get(i).getValue())) {
                remove((K) elements.get(i).getKey());
            }
        }
    }

    public V get(K key) {
        if (keys.contains(key)) {
            Predicate<Entry> predicate = (s) -> s.getKey().equals(key);
            Entry entry = new Entry(elements.search(predicate));
            return (V) entry.getValue();
        }
        return null;
    }

    public boolean containsValue(V storage) {
        Predicate<Entry> predicate = (p) -> p.getValue().equals(storage);
        return elements.search(predicate) != null;
    }

    public K getKeyByValue(V storage) {
        for (int i = 0; i <= elements.size(); i++) {
            Predicate<Entry> predicate = (p) -> p.getValue().equals(storage);
            if (containsValue(storage)) {
                return (K) elements.search(predicate).getKey();
            }
        }
        return null;
    }

    private Entry getEntry(K key) {
        if (keys.contains(key)) {
            Predicate<Entry> predicate = (s) -> s.getKey().equals(key);
            return elements.search(predicate);
        }
        return null;
    }

    private class Entry<K, V> {
        private K key;
        private V value;

        private Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        private Entry(Entry entry) {
            key = (K) entry.getKey();
            value = (V) entry.getValue();
        }

        private K getKey() {
            return key;
        }

        private void setKey(K key) {
            this.key = key;
        }

        private V getValue() {
            return value;
        }

        private void setValue(V value) {
            this.value = value;
        }

        @Override
        public int hashCode() {
            char[] keyArr = key.toString().toCharArray();
            int hashCode = 0;
            for (int i = 0; i < keyArr.length; i++) {
                hashCode += keyArr[i] * 31 ^ (keyArr.length - i + 1);
            }
            return hashCode;
        }

        @Override
        public boolean equals(Object obj) {
            Entry innerEntry = (Entry) obj;
            return (key.equals(innerEntry.getKey())) && (value.equals(innerEntry.getValue()));
        }
    }
}
