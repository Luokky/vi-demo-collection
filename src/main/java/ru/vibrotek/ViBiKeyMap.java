package ru.vibrotek;

import java.util.ArrayList;
import java.util.List;

/**
 * Карта должна хранить в себе только одно значение для пары ключей
 * (key1, key2) - пара ключей определяет уникальность значения (порядок ключей не должен учитываться)
 * для одного ключа может быть много значений
 */
public class ViBiKeyMap<T> {
    private ViList elements = new ViList();

    public T get(Object key1, Object key2) {
        if (elements.size()>0){
            for (int i = 0; i < elements.size(); i++){
                Entry entry = (Entry) elements.get(i);
                if (buildSymmetricHash(key1, key2) == buildSymmetricHash(entry.getKey1(), entry.getKey2()))
                    return (T) entry.getValue();
            }
        }
        return null;
    }

    /**
     * Возвращает все значения по одному ключу
     * @param key
     * @return
     */
    public List<T> getValuesByCategory(Object key) {
        if (elements.size()>0) {
            ArrayList result = new ArrayList();
            for (int i = 0; i<elements.size(); i++){
                Entry entry = (Entry) elements.get(i);
                if (key.equals(entry.getKey1()) || key.equals(entry.getKey2())){
                    result.add(((Entry) elements.get(i)).getValue());
                }
            }
            return result;
        }
        return null;
    }

    /**
     * Удаляет все значения к которым ведет ключ
     * @param key
     */
    public void removeByCategoryDeep(Object key) {
        if (elements.size()>0){
            for (int i = elements.size()-1; i>=0; i--){
                Entry entry = (Entry) elements.get(i);
                if (key.equals(entry.getKey1()) || key.equals(entry.getKey2())){
                    elements.remove(i);
                }
            }
        }
    }

    public void add(T value, Object key1, Object key2) {
        if (elements.size()==0) {
            elements.add(new Entry(key1, key2, value));
        } else {
            if (!haveEqual(key1,key2)){
                elements.add(new Entry(key1, key2, value));
            }
        }

    }

    public List<T> getAll() {
        ArrayList result = new ArrayList();
        for (int i = 0; i<elements.size(); i++){
            result.add(((Entry)elements.get(i)).getValue());
        }
        return result;
    }

    private boolean haveEqual(Object key1, Object key2){
        if (elements.size()>0){
            for (int i = 0; i< elements.size(); i++){
                Entry entry = (Entry) elements.get(i);
                if (buildSymmetricHash(entry.getKey1(), entry.getKey2()) == buildSymmetricHash(key1, key2)) {
                        return true;
                }
            }
        }
        return false;
    }

    private int buildSymmetricHash(Object key1, Object key2) {
        char[] keyArr1 = key1.toString().toCharArray();
        char[] keyArr2 = key2.toString().toCharArray();
        int hashCodeForKey1 = 17;
        for (int i = 0; i< keyArr1.length; i++){
            hashCodeForKey1 = hashCodeForKey1 + 7 * (int)keyArr1[i];
        }
        int hashCodeForKey2 = 17;
        for (int i = 0; i< keyArr2.length; i++){
            hashCodeForKey2 = hashCodeForKey2 + 7 * (int)keyArr2[i];
        }
        int result = (hashCodeForKey1+1)*(hashCodeForKey2+1);
        return result;
    }

    private class Entry<T>{
        private Object key1;
        private Object key2;
        private T value;

        private Entry(Object key1, Object key2, T value){
            this.key1 = key1;
            this.key2 = key2;
            this.value = value;
        }

        private Entry (Entry entry){
            key1 = entry.getKey1();
            key2 = entry.getKey2();
            value = (T) entry.getValue();
        }


        private Object getKey1() {
            return key1;
        }

        private Object getKey2() {
            return key2;
        }

        private T getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            return buildSymmetricHash(key1,key2);
        }

        @Override
        public boolean equals(Object obj) {
            return hashCode() == obj.hashCode();
        }
    }

}
