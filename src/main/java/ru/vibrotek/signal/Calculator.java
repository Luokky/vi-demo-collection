package ru.vibrotek.signal;

import com.sun.deploy.util.ArrayUtil;

/**
 *
 */
public class Calculator {

    private int sum = 0;
    private int numberOfSignals = 0;
    private byte[] endOfLastBlock = new byte[]{};

    public float processSignal(byte[] signal) {

        float averageDistance = 0;

        byte[] searchArea = new byte[endOfLastBlock.length+signal.length];
        System.arraycopy(endOfLastBlock, 0, searchArea, 0, endOfLastBlock.length);
        System.arraycopy(signal, 0, searchArea, endOfLastBlock.length, signal.length);

        int firstLoc = findMarkLoc(0, searchArea);
        if (firstLoc>=0){
            int distance = identifyDistance(firstLoc, searchArea);
            if (distance>0){
                sum+=distance;
                numberOfSignals++;
                byte[] newArea = new byte[searchArea.length-distance-firstLoc];
                System.arraycopy(searchArea, firstLoc+distance, newArea, 0,newArea.length);
                searchArea = newArea;
                endOfLastBlock = searchArea;
                if (identifyDistance(0,searchArea)>0) {
                    endOfLastBlock = new byte[]{};
                    processSignal(searchArea);
                }
            }
        }
        averageDistance = sum/numberOfSignals;
        return averageDistance;
    }

    public void test() {
        byte[] signalBlock1 = {0,0,1,1,1,0,0,0,0,1,1,1,0,0,0,0};//тут растояние между метками 7 отсчетов
        byte[] signalBlock2 = {0,0,0,0,1,1,1,1,0,0,0,0,1};
        //между меткой из предыдущего куска сигнала и первой в этом блоке 11 отсчетов
        // между первой и второй меткой в этом блоке 8 отсчетов
        byte[] signalBlock3 = {1,1,0,0,0,1,1,1,1};//тут 6 отсчетов
        float averageDistance1 = processSignal(signalBlock1);//на этом шаге должны получить среднее averageDistance1=7
        System.out.println("averageDistance1 = " + averageDistance1);
        float averageDistance2 = processSignal(signalBlock2);//на этом шаге должны получить averageDistance2=(7+11)/2=9
        System.out.println("averageDistance2 = " + averageDistance2);
        // хотя в конце signalBlock2 стоит 1, но мы незнаем будет ли метка, т.к. следущий блок может начаться с 0
        float averageDistance3 = processSignal(signalBlock3);//на этом шаге должны получить averageDistance2=(7+11+8+6)/4=8
        //мы получили signalBlock3 и поняли что метка началась еще в конце signalBlock2
        System.out.println("averageDistance3 = " + averageDistance3);
    }

    private int findMarkLoc(int startIndex, byte[] signal){
        for (int i = startIndex; i < signal.length; i++){
            if (signal[i]==1 && signal.length > i+2){
                if (signal[i+1]==1&&signal[i+2]==1) return i;
            }
        }
        return -1;
    }

    private int identifyMarkLength (int markStart, byte[] signal){
        int markLength = 0;
        for (int i = markStart; i < signal.length; i++){
            if (signal[i]==1) markLength++;
            else return markLength;
        }
        return markLength;
    }

    private int identifyDistance(int markStart, byte[] signal){
        int markLength = identifyMarkLength(markStart, signal);
        int nextMarkStart = findMarkLoc(markStart+markLength, signal);
        if(nextMarkStart>0) return nextMarkStart-markStart;
        return -1;
    }


}
