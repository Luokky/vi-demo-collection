package ru.vibrotek;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class ViMapTest {
    public void startTest() {

        //Проверка add
        ViMap viMapTest = new ViMap();
        viMapTest.add(0, "zero");
        viMapTest.add(4, "zero");
        viMapTest.add(1, "one");
        viMapTest.add(2, "two");
        viMapTest.add(3, "three");
        viMapTest.add(5, "zero");
        viMapTest.add(6, "two");
        viMapTest.add(7, "test");

        //Проверка перезаписи значения по существующему ключу + Проверка forEachKeys & forEachValues
        viMapTest.put(1, "The First");
        printAllKeys(viMapTest);
        printAllValues(viMapTest);
        System.out.println();

        //Проверка удаления по ключу
        viMapTest.remove(1);
        printAllKeys(viMapTest);
        printAllValues(viMapTest);
        System.out.println();

        //Проверка  getKeyByValue
        System.out.println("get key by value \"zero\"");
        System.out.println(viMapTest.getKeyByValue("zero"));
        System.out.println("get key by value \"smth strange\"");
        System.out.println(viMapTest.getKeyByValue("smth strange"));
        System.out.println();

        //Проверка удаления по предикату
        Predicate<String> predicate = s -> s.equals("zero");
        viMapTest.removeIf(predicate);
        printAllKeys(viMapTest);
        printAllValues(viMapTest);
    }

    private void printAllKeys(ViMap vm) {
        System.out.print("All Keys: ");
        Consumer<Integer> printKeys = s -> System.out.print(s + " ");
        vm.forEachKey(printKeys);
        System.out.println();
    }

    private void printAllValues(ViMap vm) {
        System.out.print("All Values: ");
        Consumer<String> printValues = s -> System.out.print(s + " ");
        vm.forEachValue(printValues);
        System.out.println();
    }
}
